# Spring Boot Workshop - Beispielprojekt

Im Verlauf eines von mir ausgetragenen, mehrteiligen Online-Workshops zum Thema Spring Boot erstelltes Beispielprojekt.

## Behandelte Themenkomplexe
1. Erstellen eines Spring Boot Projektes mit dem [Spring Initializer](https://start.spring.io)
2. Objekorientiertes Design in Java + Einführung in Project Lombok
3. Einführung in Spring Boot: Architektur, Spring Beans, Dependency Injection, Autokonfiguration
4. Persistenz mit JPA/Hibernate & Spring Data JPA
5. Businesslogik mit Spring Services
6. REST-APIs & API-Design mit Spring MVC REST + Exception Handling
7. Spring REST Repositories
8. Validierung mit Bean Validation
9. Testing in Spring Boot: JUnit 5, Mockito, Spring Data JPA Test, Mock MVC

## Geplante Erweiterungen
* API-Dokumentation: Swagger/OpenAPI, Spring REST Docs
* fortgeschrittene Themenkomplexe (je nach Teilnehmerwunsch), z.B.
    * Spring Profile
    * Spring Konfigurationsklassen / Erstellen von benutzerdefinierten Beans
    * erweiterte JPA-Themen, z.B. Verwendung eines komplexen Objektes als Primärschlüssel
    * DB-Anbindung an MySQL
    * DB-Anbindung an MongoDB
    * "REST-APIs Level 4": Hypermedia/HATEOAS mit Spring MVC REST
    * REST Services liefern XML aus (neben oder anstatt JSON)
    * Konsumieren einer REST-API mit Spring REST Template
    * Konsumieren einer REST-API mit React oder Angular
    * serverseitig gerenderte Webpages mit Spring MVC & Thymeleaf
    * Deployment Spring Application mit Docker
    * ...