package de.fabianmittmann.springboot2demo.support.rest;

import de.fabianmittmann.springboot2demo.student.StudentNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author Fabian Mittmann
 */
@ControllerAdvice
public class RestExceptionHandling {

    @ExceptionHandler
    public ResponseEntity<String> studentNotFoundHandling(StudentNotFoundException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }

}
