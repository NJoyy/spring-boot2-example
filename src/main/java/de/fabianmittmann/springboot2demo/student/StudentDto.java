package de.fabianmittmann.springboot2demo.student;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import de.fabianmittmann.springboot2demo.courses.CourseEntity;
import de.fabianmittmann.springboot2demo.localization.Address;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Null;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Fabian Mittmann
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class StudentDto {
    @Null
    private Long id;

    @Size(min = 2, max = 100)
    @NotEmpty
    private String firstName;

    @Size(min = 2, max = 100)
    @NotEmpty
    private String lastName;

    @PastOrPresent
    private LocalDate birthday;

    private Address address;

    @JsonIgnoreProperties("students")
    private Set<CourseEntity> courses = new HashSet<>();
}
