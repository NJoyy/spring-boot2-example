package de.fabianmittmann.springboot2demo.student;

import lombok.NonNull;

/**
 * @author Fabian Mittmann
 */
public class StudentNotFoundException extends Exception {
    StudentNotFoundException(@NonNull Long studentId) {
        super("student with id " + studentId + " could not be found");
    }
}
