package de.fabianmittmann.springboot2demo.student;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

/**
 * @author Fabian Mittmann
 */
@Repository
@RequiredArgsConstructor
public class StudentRepository {
    private final EntityManager entityManager;

    public StudentEntity save(StudentEntity student) {
        if (student.getId() == null) {
            entityManager.persist(student);
        } else {
            findById(student.getId()).ifPresentOrElse(
                    x -> entityManager.merge(student),
                    () -> entityManager.persist(student)
            );
        }
        return student;
    }

    public void deleteById(Long id) {
        findById(id).ifPresent(entityManager::remove);
    }

    public Optional<StudentEntity> findById(Long id) {
        return Optional.ofNullable(entityManager.find(StudentEntity.class, id));
    }

    public List<StudentEntity> findAll() {
        return entityManager
                .createQuery("SELECT s FROM StudentEntity s", StudentEntity.class)
                .getResultList();
    }

}
