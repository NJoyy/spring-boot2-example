package de.fabianmittmann.springboot2demo.student;

import de.fabianmittmann.springboot2demo.courses.CourseEntity;
import de.fabianmittmann.springboot2demo.localization.Address;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Fabian Mittmann
 */
@Entity
@Table(name = "STUDENTS")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString(exclude = "courses")
public class StudentEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "FIRST_NAME", length = 50, nullable = false, updatable = false)
    private String firstName;
    private String lastName;
    private LocalDate birthday;

    @Embedded
    private Address address;

    @ManyToMany
    @JoinTable(
        name = "STUDENTS_COURSES",
        joinColumns = @JoinColumn(name = "STUDENT_ID"),
        inverseJoinColumns = @JoinColumn(name = "COURSE_ID")
    )
    private Set<CourseEntity> courses = new HashSet<>();
}
