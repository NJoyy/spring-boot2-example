package de.fabianmittmann.springboot2demo.student;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Fabian Mittmann
 */
@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentDataRestController {
    private final StudentDataService studentDataService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<StudentDto> getAll() {
        return studentDataService.getAll();
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public StudentDto getById(@PathVariable("id") String id) throws StudentNotFoundException {
        return studentDataService.getById(Long.valueOf(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public StudentDto postStudent(@RequestBody @Valid StudentDto student) {
        return studentDataService.save(student);
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public StudentDto putStudent(@PathVariable("id") String id,
                                 @RequestBody @Valid StudentDto student) throws StudentNotFoundException {
        return studentDataService.handlePut(Long.valueOf(id), student);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStudent(@PathVariable("id") String id) throws StudentNotFoundException {
        studentDataService.deleteById(Long.valueOf(id));
    }
}
