package de.fabianmittmann.springboot2demo.student;

import lombok.NonNull;
import org.springframework.stereotype.Component;

/**
 * @author Fabian Mittmann
 */
@Component
public class StudentTransformer {
    public StudentDto toDto(@NonNull StudentEntity entity) {
        return StudentDto.builder()
                .id(entity.getId())
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .birthday(entity.getBirthday())
                .address(entity.getAddress())
                .courses(entity.getCourses())
                .build();
    }

    public StudentEntity toEntity(@NonNull StudentDto dto) {
        return StudentEntity.builder()
                .id(dto.getId())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .birthday(dto.getBirthday())
                .address(dto.getAddress())
                .courses(dto.getCourses())
                .build();
    }
}
