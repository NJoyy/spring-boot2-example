package de.fabianmittmann.springboot2demo.student;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Fabian Mittmann
 */
@Service
@RequiredArgsConstructor
public class StudentDataServiceImpl implements StudentDataService {
    private final StudentRepository studentRepository;
    private final StudentTransformer studentTransformer;

    @Transactional
    @Override
    public StudentDto save(@NonNull StudentDto student) {
        return studentTransformer
                .toDto(studentRepository.save(studentTransformer.toEntity(student)));
    }

    @Transactional
    @Override
    public void deleteById(@NonNull Long id) throws StudentNotFoundException {
        if (studentRepository.findById(id).isEmpty()) {
            throw new StudentNotFoundException(id);
        }
        studentRepository.deleteById(id);
    }

    @Transactional
    @Override
    public StudentDto getById(@NonNull Long id) throws StudentNotFoundException {
        return studentRepository
                .findById(id)
                .map(studentTransformer::toDto)
                .orElseThrow(() -> new StudentNotFoundException(id));
    }

    @Transactional
    @Override
    public List<StudentDto> getAll() {
        return studentRepository.findAll()
                .stream()
                .map(studentTransformer::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public StudentDto handlePut(@NonNull Long id,
                                @NonNull StudentDto studentUpdateData) throws StudentNotFoundException {
        if (studentRepository.findById(id).isEmpty()) {
            throw new StudentNotFoundException(id);
        } else {
            studentUpdateData.setId(id);
            return studentTransformer.toDto(studentRepository.save(studentTransformer.toEntity(studentUpdateData)));
        }
    }
}
