package de.fabianmittmann.springboot2demo.student;

import java.util.List;

/**
 * @author Fabian Mittmann
 */
public interface StudentDataService {
    StudentDto save(StudentDto student);
    void deleteById(Long id) throws StudentNotFoundException;
    StudentDto getById(Long id) throws StudentNotFoundException;
    List<StudentDto> getAll();
    StudentDto handlePut(Long id, StudentDto studentUpdateData) throws StudentNotFoundException;
}
