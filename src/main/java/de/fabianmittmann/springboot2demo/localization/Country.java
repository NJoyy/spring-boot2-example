package de.fabianmittmann.springboot2demo.localization;

/**
 * @author Fabian Mittmann
 *
 * Supported locations in the application.
 */
public enum Country {
    AUSTRIA, GERMANY, SWISS
}
