package de.fabianmittmann.springboot2demo.localization;

import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * @author Fabian Mittmann
 */
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Address {
    private String street;
    private String houseNumber;
    private String zipCode;
    private String city;

    @Enumerated(EnumType.STRING)
    private Country country;
}
