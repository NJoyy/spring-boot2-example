package de.fabianmittmann.springboot2demo;

import de.fabianmittmann.springboot2demo.courses.CourseEntity;
import de.fabianmittmann.springboot2demo.courses.CourseRepository;
import de.fabianmittmann.springboot2demo.localization.Address;
import de.fabianmittmann.springboot2demo.localization.Country;
import de.fabianmittmann.springboot2demo.student.StudentDataService;
import de.fabianmittmann.springboot2demo.student.StudentDto;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Set;

/**
 * @author Fabian Mittmann
 */
@Profile("dev")
@Component
@RequiredArgsConstructor
public class DataInitializer implements CommandLineRunner {
    private final CourseRepository courseRepository;
    private final StudentDataService studentDataService;

    @Override
    public void run(String... args) throws Exception {
        StudentDto fabi = StudentDto.builder()
                .firstName("Fabian")
                .lastName("Mittmann")
                .birthday(LocalDate.of(1997, 7, 31))
                .address(
                        Address.builder()
                                .city("Leipzig")
                                .houseNumber("1a")
                                .street("Am Bahnhof")
                                .zipCode("04159")
                                .country(Country.GERMANY)
                                .build()
                ).build();

        StudentDto fabiSaved = studentDataService.save(fabi);

        CourseEntity course = CourseEntity.builder()
                .title("Algorithmen")
                .maxParticipants(100)
                .build();
        courseRepository.save(course);

        fabiSaved.setCourses(Set.of(course));
        studentDataService.save(fabiSaved);
    }
}
