package de.fabianmittmann.springboot2demo.courses;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Fabian Mittmann
 */
public interface CourseRepository extends JpaRepository<CourseEntity, Long> {
}
