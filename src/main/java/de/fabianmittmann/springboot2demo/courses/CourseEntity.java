package de.fabianmittmann.springboot2demo.courses;

import de.fabianmittmann.springboot2demo.student.StudentEntity;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Fabian Mittmann
 */
@Entity
@Table(name = "COURSES")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString(exclude = "students")
public class CourseEntity {
    @Id
    @GeneratedValue
    private Long id;

    private String title;
    private int maxParticipants;

    @ManyToMany(mappedBy = "courses")
    private Set<StudentEntity> students = new HashSet<>();
}
