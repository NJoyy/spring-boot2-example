package de.fabianmittmann.springboot2demo.student;

import de.fabianmittmann.springboot2demo.courses.CourseEntity;
import de.fabianmittmann.springboot2demo.localization.Address;
import de.fabianmittmann.springboot2demo.localization.Country;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author Fabian Mittmann
 */
class StudentDataServiceImplTest {

    @InjectMocks
    private StudentDataServiceImpl uut;

    @Mock
    private StudentRepository studentRepository;

    @Mock
    private StudentTransformer studentTransformer;

    private StudentDto testDto;

    private StudentEntity testEntity;

    private Long testId;
    private String testFirstName;
    private String testLastName;
    private LocalDate testDate;
    private Address testAddress;
    private Set<CourseEntity> testCourses;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        testId = 1L;
        testFirstName = "Jon";
        testLastName = "Doe";
        testDate = LocalDate.now();
        testAddress = Address.builder()
                            .city("Leipzig")
                            .houseNumber("2")
                            .street("Street")
                            .zipCode("01234")
                            .country(Country.GERMANY)
                            .build();
        testCourses = Set.of(CourseEntity.builder().id(testId).build());

        testDto = StudentDto.builder()
                .id(testId)
                .firstName(testFirstName)
                .lastName(testLastName)
                .birthday(testDate)
                .address(testAddress)
                .courses(testCourses)
                .build();

        testEntity = StudentEntity.builder()
                .id(testId)
                .firstName(testFirstName)
                .lastName(testLastName)
                .birthday(testDate)
                .address(testAddress)
                .courses(testCourses)
                .build();
    }

    @Test
    void save() {
        when(studentRepository.save(any())).thenReturn(testEntity);
        when(studentTransformer.toDto(any())).thenReturn(testDto);
        when(studentTransformer.toEntity(any())).thenReturn(testEntity);

        StudentDto result = uut.save(testDto);

        assertNotNull(result);
        assertEquals(testId, result.getId());
        assertEquals(testFirstName, result.getFirstName());
        assertEquals(testLastName, result.getLastName());
        assertEquals(testDate, result.getBirthday());
        assertEquals(testAddress, result.getAddress());
        assertEquals(testCourses.size(), result.getCourses().size());
    }

    @Test
    void handlePut() throws Exception {
        when(studentRepository.findById(anyLong())).thenReturn(Optional.of(testEntity));
        when(studentRepository.save(any())).thenReturn(testEntity);
        when(studentTransformer.toDto(any())).thenReturn(testDto);
        when(studentTransformer.toEntity(any())).thenReturn(testEntity);

        StudentDto result = uut.handlePut(testId, testDto);

        assertNotNull(result);
        assertEquals(testId, result.getId());
        assertEquals(testFirstName, result.getFirstName());
        assertEquals(testLastName, result.getLastName());
        assertEquals(testDate, result.getBirthday());
        assertEquals(testAddress, result.getAddress());
        assertEquals(testCourses.size(), result.getCourses().size());
    }

    @Test
    void deleteById() throws Exception {
        when(studentRepository.findById(anyLong())).thenReturn(Optional.of(testEntity));
        uut.deleteById(testId);

        verify(studentRepository, times(1)).deleteById(anyLong());
    }

    @Test
    void getById() throws Exception {
        when(studentRepository.findById(anyLong())).thenReturn(Optional.of(testEntity));
        when(studentTransformer.toDto(any())).thenReturn(testDto);
        StudentDto result = uut.getById(testId);

        assertNotNull(result);
        assertEquals(testId, result.getId());
        assertEquals(testFirstName, result.getFirstName());
        assertEquals(testLastName, result.getLastName());
        assertEquals(testDate, result.getBirthday());
        assertEquals(testAddress, result.getAddress());
        assertEquals(testCourses.size(), result.getCourses().size());
    }

    @Test
    void getAll() {
        when(studentRepository.findAll()).thenReturn(List.of(testEntity));
        when(studentTransformer.toDto(any())).thenReturn(testDto);
        List<StudentDto> resultList = uut.getAll();

        assertNotNull(resultList);
        assertFalse(resultList.isEmpty());
        assertEquals(1, resultList.size());
    }
}