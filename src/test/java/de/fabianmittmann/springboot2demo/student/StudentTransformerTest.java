package de.fabianmittmann.springboot2demo.student;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Fabian Mittmann
 */
class StudentTransformerTest {
    private StudentTransformer uut;

    private StudentEntity testEntity;
    private StudentDto testDto;
    private Long testId;
    private String testFirstName;
    private String testLastName;
    private LocalDate testDate;

    @BeforeEach
    void setUp() {
        uut = new StudentTransformer();

        testId = 1L;
        testFirstName = "Hey";
        testLastName = "Joe";
        testDate = LocalDate.now();
        testEntity = StudentEntity.builder()
                .id(testId)
                .firstName(testFirstName)
                .lastName(testLastName)
                .birthday(testDate)
                .build();

        testDto = StudentDto.builder()
                .id(testId)
                .firstName(testFirstName)
                .lastName(testLastName)
                .birthday(testDate)
                .build();
    }

    @Test
    void toDto() {
        StudentDto result = uut.toDto(testEntity);

        assertEquals(testId, result.getId());
        assertEquals(testFirstName, result.getFirstName());
        assertEquals(testLastName, result.getLastName());
        assertEquals(testDate, result.getBirthday());
    }

    @Test
    void toDto_withNullParam() throws Exception {
        assertThrows(NullPointerException.class, () -> uut.toDto(null));
    }

    @Test
    void toEntity() {
        StudentEntity result = uut.toEntity(testDto);

        assertEquals(testId, result.getId());
        assertEquals(testFirstName, result.getFirstName());
        assertEquals(testLastName, result.getLastName());
        assertEquals(testDate, result.getBirthday());
    }

    @Test
    void toEntity_withNullParam() throws Exception {
        assertThrows(NullPointerException.class, () -> uut.toEntity(null));
    }
}