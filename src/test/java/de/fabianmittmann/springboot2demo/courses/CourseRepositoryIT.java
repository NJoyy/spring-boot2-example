package de.fabianmittmann.springboot2demo.courses;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Fabian Mittmann
 */
@DataJpaTest
class CourseRepositoryIT {
    @Autowired
    private CourseRepository courseRepository;
    private CourseEntity testEntity;

    @BeforeEach
    void setUp() throws Exception {
        testEntity = CourseEntity.builder().title("Test").maxParticipants(50).build();
    }

    @Test
    void save() throws Exception {
        assertTrue(courseRepository.findAll().isEmpty());

        CourseEntity courseAfterSave = courseRepository.save(testEntity);

        assertNotNull(courseAfterSave);
        assertNotNull(courseAfterSave.getId());
        assertFalse(courseRepository.findAll().isEmpty());
        assertTrue(courseRepository.findById(courseAfterSave.getId()).isPresent());
    }

    @Test
    void delete() throws Exception {
        CourseEntity courseAfterSave = courseRepository.save(testEntity);
        assertTrue(courseRepository.findById(courseAfterSave.getId()).isPresent());

        courseRepository.delete(testEntity);
        assertFalse(courseRepository.findById(courseAfterSave.getId()).isPresent());
    }
}