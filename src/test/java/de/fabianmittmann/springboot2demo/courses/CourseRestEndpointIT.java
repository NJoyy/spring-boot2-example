package de.fabianmittmann.springboot2demo.courses;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Fabian Mittmann
 */
@SpringBootTest
@AutoConfigureMockMvc
public class CourseRestEndpointIT {
    private static final String API_URL = "/courseEntities/";
    private static final String APPLICATION_HAL_JSON = "application/hal+json";
    private static final String JSON_UPDATE = "{\"maxParticipants\": 50}";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private ObjectMapper objectMapper;

    private CourseEntity testCourse;

    @BeforeEach
    void setUp() throws Exception {
        testCourse = CourseEntity.builder()
                .title("Test")
                .maxParticipants(50)
                .build();

        courseRepository.deleteAll();
    }

    @Test
    void getById() throws Exception {
        CourseEntity course = courseRepository.save(testCourse);

        mockMvc.perform(get(API_URL + course.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_HAL_JSON))
                .andExpect(jsonPath("$.title", equalTo("Test")))
                .andExpect(jsonPath("$.maxParticipants", equalTo(50)));
    }

    @Test
    void getAll() throws Exception {
        courseRepository.save(testCourse);

        mockMvc.perform(get(API_URL).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_HAL_JSON))
                .andExpect(jsonPath("$._embedded.courseEntities", hasSize(1)))
                .andExpect(jsonPath("$._embedded.courseEntities[0].title", equalTo("Test")))
                .andExpect(jsonPath("$._embedded.courseEntities[0].maxParticipants", equalTo(50)));
    }

    @Test
    void postCourse() throws Exception {
        mockMvc.perform(post(API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(testCourse))
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title", equalTo("Test")))
                .andExpect(jsonPath("$.maxParticipants", equalTo(50)));
    }

    @Test
    void updateCourseWithPut() throws Exception {
        CourseEntity course = courseRepository.save(testCourse);

        mockMvc.perform(put(API_URL + course.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(JSON_UPDATE)
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title", equalTo(null)))
                .andExpect(jsonPath("$.maxParticipants", equalTo(50)));
    }

    @Test
    void updateCourseWithPatch() throws Exception {
        CourseEntity course = courseRepository.save(testCourse);

        mockMvc.perform(patch(API_URL + course.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(testCourse))
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title", equalTo("Test")))
                .andExpect(jsonPath("$.maxParticipants", equalTo(50)));
    }

    @Test
    void deleteCourse() throws Exception {
        CourseEntity course = courseRepository.save(testCourse);
        assertTrue(courseRepository.findById(course.getId()).isPresent());

        mockMvc.perform(delete(API_URL + course.getId()))
                .andExpect(status().isNoContent());

        assertTrue(courseRepository.findById(course.getId()).isEmpty());
    }
}
